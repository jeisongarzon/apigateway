const { gql } = require('apollo-server');
const commentsTypeDefs = gql `
type comments {
id: String!
commentContent: String!
idUserFK: String!
userName: String!
idPostFK: String!
dateComment: String!
}
input commentsInput{
    commentContent: String!
    idUserFK: String!
    userName: String!
    idPostFK: String!
}
extend type Query {
commentsByUsername(userName: String!): comments
}

extend type Mutation{
    createComment(commentI : commentsInput!
        ): comments
}
`;
module.exports = commentsTypeDefs;