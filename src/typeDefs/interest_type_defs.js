const { gql } = require('apollo-server');
const interestTypeDefs = gql `
type comments {
id: String!
idUserFK: String!
idPostFK: String!
interestDate: String!
userName: String!
}
input interestInput{
    activate: Boolean!
}
extend type Query {
interestByUsername(userName: String!): interest
}
extend type Mutation{
    createInterest(interestC: activate): createI
}
`;
module.exports = interestTypeDefs;