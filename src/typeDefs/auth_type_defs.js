const { gql } = require('apollo-server');
const authTypeDefs = gql `
type Tokens {
refresh: String!
access: String!
}
type Access {
access: String!
}
input CredentialsInput {
username: String!
password: String!
}
input SignUpInput {
nameUser: String!
lastNameUser: String!
username: String!
password: String!
emailUser: String!
cellphoneUser: Int!
dateBirthUser: String!
genderUser: String!
statusUser: Boolean!
useType: String!
imageUser: String!
}
type UserDetail {
id: Int!
nameUser: String!
lastNameUser: String!
username: String!
password: String!
emailUser: String!
cellphoneUser: Int!
dateBirthUser: String!
genderUser: String!
statusUser: Boolean!
useType: String!
imageUser: String!
}
input UserDeleteInput {
nameUser: String!
} 
input UserUpdateInput {
nameUser: String
lastNameUser: String
username: String
password: String
emailUser: String
cellphoneUser: Int
dateBirthUser: String
genderUser: String
statusUser: Boolean
useType: String
imageUser: String
} 
type Mutation {
signUpUser(userInput :SignUpInput): Tokens!
logIn(credentials: CredentialsInput!): Tokens!
refreshToken(refresh: String!): Access!
 UserDelete(DeleteU: UserDeleteInput!): Delete!
UserUpdate(UpdateU: UserUpdateInput!): Update!
}
type Query {
userDetailById(userId: Int!): UserDetail!
}

`;
module.exports = authTypeDefs;